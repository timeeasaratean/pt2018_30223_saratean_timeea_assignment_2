package maven;
public class Client {
	public int arrivalTime;
	public int simulationTime;
	public Client( int arrivalTime, int simulationTime) {
	
		this.arrivalTime=arrivalTime;
		this.simulationTime=simulationTime;
	}
	public int getArrivalTime() {
		return arrivalTime;
	}
	public int getSimulationTime() {
		return simulationTime;
	}
	public void setSimulationTime(int simulationTime) {
		this.simulationTime=simulationTime;
	}
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime=arrivalTime;
	}
	public String toString() {
		String s="";
		if (this.getArrivalTime()!=0)
			s+=(getArrivalTime() + "," + getSimulationTime());
		return s;
	}
}
