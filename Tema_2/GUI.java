package maven;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class GUI extends JComponent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int clientii, mina, maxa, mins, maxs,cozi,timp;
	private static File file = new File("log.txt");
	private static FileWriter fw;  
	protected static BufferedWriter bw;
	protected static JFrame frame=new JFrame();
	 JPanel pan=new JPanel();
	 JLabel clients= new JLabel ("Number of Clients: ") ;
	 JTextField noCL = new JTextField () ;
	 JLabel minArrival= new JLabel ("Minimum arrival time: ") ;
	 JTextField minA = new JTextField () ;
	 JLabel maxArrival= new JLabel ("Maximum arrival time: ") ;
	 JTextField maxA = new JTextField () ;
	 JLabel minSimulation= new JLabel ("Minimum simulation time: ") ;
	 JTextField minS = new JTextField () ;
	 JLabel maxSimulation= new JLabel ("Maximum simulation time: ") ;
	 JTextField maxS = new JTextField () ;
	 JLabel numberOfQueues= new JLabel ("Number of Queues: ") ;
	 JTextField queues = new JTextField () ;
	 JLabel timeLimit= new JLabel ("Time limit: ") ;
	 JTextField time = new JTextField () ;
	 JButton start=new JButton("Start");
	 JLabel pick=new JLabel("Pick a strategy: ");
	 String[] strategies= {"Time","Queue"};
	 JComboBox strategy=new JComboBox(strategies);
	 static int AREA_ROWS = 10;
	 static int AREA_COLUMNS = 30;

	 static JTextArea textArea = new JTextArea(
	          AREA_ROWS, AREA_COLUMNS);
	 static JTextArea textArea2=new JTextArea(AREA_ROWS,AREA_COLUMNS);
	   
	 JScrollPane scrollPane = new JScrollPane(textArea);
	 JScrollPane scrollPane1 = new JScrollPane(textArea2);
	 public GUI() {
		 try{
				fw = new FileWriter(file.getAbsoluteFile());
				if (!file.exists()) {
					file.createNewFile();
				}
				bw = new BufferedWriter(fw);
			}
			catch(IOException e1){
				
			}
		 	frame=new JFrame("Queues");
			frame.setBounds(100, 100, 650, 900);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			pan.setLayout(null);
			frame.setContentPane(pan);
			frame.setResizable(false);
			
		
			minArrival.setBounds(140, 10, 150, 25);
		
			maxArrival.setBounds(340, 10, 150, 25);
	
			minSimulation.setBounds(140, 70, 150, 25);

			maxSimulation.setBounds(340, 70, 160, 25);
	
			numberOfQueues.setBounds(160, 130, 150, 25);
			clients.setBounds(150, 195, 130, 30);

			timeLimit.setBounds(360, 130, 150, 25);
			frame.getContentPane().add(clients);
			frame.getContentPane().add(minArrival);
			frame.getContentPane().add(maxArrival);
			frame.getContentPane().add(minSimulation);
			frame.getContentPane().add(maxSimulation);
			frame.getContentPane().add(numberOfQueues);
			frame.getContentPane().add(timeLimit);
			

			minA.setBounds(160, 30, 100, 25);
			noCL.setBounds(150, 230, 130, 30);

			maxA.setBounds(360, 30, 100, 25);

			minS.setBounds(160, 100, 100, 25);
	
			maxS.setBounds(360, 100, 100, 25);

			queues.setBounds(160, 160, 100, 25);

			time.setBounds(360, 160, 100, 25);
			frame.getContentPane().add(minA);
			frame.getContentPane().add(noCL);
			frame.getContentPane().add(maxA);
			frame.getContentPane().add(minS);
			frame.getContentPane().add(maxS);
			frame.getContentPane().add(queues);
			frame.getContentPane().add(time);
			pick.setBounds(350,190,150,50);
			frame.getContentPane().add(pick);
			strategy.setBounds(330,230,150,30);
			frame.getContentPane().add(strategy);
			start.setBounds(250, 270, 150, 50);
			scrollPane1.setBounds(20, 630, 600, 200);
			scrollPane.setBounds(20,330,600,270);	
			frame.getContentPane().add(scrollPane);
			frame.getContentPane().add(start);
			frame.getContentPane().add(scrollPane1);
			frame.setVisible(true);
			start.addActionListener(new Start());
			strategy.addActionListener(new Combo());
	 } 
	 class Start implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	        	  mina = Integer.parseInt(minA.getText());
		    	  maxa = Integer.parseInt(maxA.getText());
				  mins = Integer.parseInt(minS.getText());
				  maxs = Integer.parseInt(maxS.getText());
				  cozi = Integer.parseInt(queues.getText());
				  timp = Integer.parseInt(time.getText());
				  clientii=Integer.parseInt(noCL.getText());
				  SimulationManager s=new SimulationManager(clientii,mina,maxa,mins,maxs,cozi,timp);
				  Thread t=new Thread(s);
				  t.start();
	        }
	    }
	 
	 class Combo implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	       	 if (strategy.getSelectedItem().toString().equals("Time"))
				{
					//Scheduler.strategy=new ConcreteStrategyTime();
					SimulationManager.selectionPolicy=SelectionPolicy.SHORTEST_TIME;
				}
				else if(strategy.getSelectedItem().toString().equals("Queue"))
				{
					SimulationManager.selectionPolicy=SelectionPolicy.SHORTEST_QUEUE;
					//Scheduler.strategy=new ConcreteStrategyQueue();
				}
	        }
	    }
	
	 } 
	

