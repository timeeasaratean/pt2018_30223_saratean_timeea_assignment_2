package maven;



import java.util.ArrayList;
import java.util.List;

public class Scheduler {
	List<Queue> queues=new ArrayList<Queue>();
	private int maxNoQueues;
	private int maxClients;
	static Strategy strategy;
	static int idCoada;
	public Scheduler(int maxNoQueues, int maxClients) {
		int i;
		for(i=0;i<maxNoQueues;i++) {
			
			Queue q=new Queue(maxClients);
			queues.add(q);
			Thread thread=new Thread(q);
			thread.start();
				
			
		}
	}
	public synchronized int getID()
	{
		return idCoada;
	}
	public void changeStrategy(SelectionPolicy policy) {
		if (policy==SelectionPolicy.SHORTEST_QUEUE) {
			strategy=new ConcreteStrategyQueue();
		}
		else {
			strategy=new ConcreteStrategyTime();
		}
	}
	public void dispatchClient(Client c) {
		strategy.addTask(queues, c);
	}
	public List<Queue> getServers()
	{
		return queues;
	}
} 

