package maven;

import java.util.List;

public interface Strategy {
	public void addTask(List<Queue> queues, Client c);
}
