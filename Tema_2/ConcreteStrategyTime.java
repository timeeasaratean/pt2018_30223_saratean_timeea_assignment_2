package maven;


import java.util.List;

public class ConcreteStrategyTime implements Strategy{
	public void addTask(List<Queue> queues, Client c) {
		int indexMinim = -1;
		int sumaMin = queues.get(0).getSuma();
		int i=0;
		for(Queue a : queues) {
			if(a.getSuma() <= sumaMin) {
				sumaMin = a.getSuma();
				indexMinim = i;
			}
			i++;
		}
		Scheduler.idCoada=indexMinim;
		for ( i=0;i<queues.size();i++)
		{
			if (indexMinim==i)
			{
				queues.get(i).addClient(c);
			}
		}
	}
}
